import numpy as np
from PIL import Image, ImageOps
import pyglet
from pyglet.gl import *
import time
import os
import cv2
import mediapipe as mp
import multiprocessing
import re
import datetime
import csv
import sys
import random
import json


TARGET_PROBABILITY = 0.7
STIMULUS_RELATIVE_SIZE = 0.7
TIME_SCALE = 1
FULLSCREEN = True
FRAME_TEXT = False
NUM_REPITITIONS = 12
NUM_CONTROL_STIMULI = 20


def prepare_stimulus(stimulus_descriptor, target):
    global prime_texture, inner_yellow_flag, outer_yellow_flag

    if stimulus_descriptor != 'c':
        prime_ix = stimulus_descriptor #np.random.choice(len(images))
        prime_img = images[prime_ix][1]
        prime_name = images[prime_ix][0]
        prime_texture = pyglet.image.ImageData(stimulus_size, stimulus_size, "RGB", prime_img.tobytes(), stimulus_size * 3 * 1)
    else:
        prime_texture = control_noise_texture
        prime_name = 'control'    

    target_stimulus = target #np.random.rand() < TARGET_PROBABILITY
    inner_yellow_flag = np.random.randint(0, 2) == 1
    outer_yellow_flag = inner_yellow_flag if target_stimulus else not inner_yellow_flag
    
    return (prime_name, (inner_yellow_flag * 1, outer_yellow_flag * 1))


def update(dt):
    global frame_float, blank_timer, Enter_pressed, A_pressed, prev_A_pressed, Sp_pressed, prev_Sp_pressed, prime_shown, target_shown, snapshot_taken, snap_val, info_arr, target_time, control_phase, stimulus_counter, initial_screen, control_finish_screen, pause_screen, thanks_screen, skip_space, prev_info_screen, pause_corrs

    if initial_screen or control_finish_screen or pause_screen:
        if not Enter_pressed:
            return
        else:
            if pause_screen:
                pause_corrs.append((corr_arr[0], corr_arr[1], corr_arr[2]))
            initial_screen, control_finish_screen, pause_screen = False, False, False
            prev_info_screen = True
            blank_timer = time.time() + 0.5
            skip_space = True

    if thanks_screen:
        if not Enter_pressed:
            return
        else:
            pyglet.app.exit()
            return

    if frame_float >= 0:
        if target_shown:
            if target_time == -1:
                target_time = time.time()
            if A_pressed and not prev_A_pressed:
                reaction_time_val.value = int(1000 * (time.time() - target_time))
                snap_null_val.value = False
                snap_val.value = 1
                snapshot_taken = True
                if inner_yellow_flag == outer_yellow_flag:
                    frame_float = -1
            if Sp_pressed and not prev_Sp_pressed:
                frame_float = -1
                if not snapshot_taken:
                    snap_null_val.value = 1
                    snap_val.value = 1
    else:
        if blank_timer == -1:
            blank_timer = 0#time.time() + 1
        elif time.time() >= blank_timer and ((not A_pressed and not Sp_pressed and prev_Sp_pressed) or skip_space):

            skip_space = False

            if control_phase and stimulus_counter >= len(control_order):
                control_phase = False
                stimulus_counter = 0
                control_finish_screen = True
                return

            if not prev_info_screen and not control_phase and stimulus_counter > 0 and stimulus_counter < len(stimuli_order) and stimulus_counter % (len(stimuli_order) // 4) == 0:
                pause_screen = True
                return

            if not control_phase and stimulus_counter >= len(stimuli_order):
                thanks_screen = True
                return

            prev_info_screen = False

            time.sleep(.5)
            
            if control_phase:
                stimulus_descriptor, target = control_order[stimulus_counter]
            else:
                stimulus_descriptor, target = stimuli_order[stimulus_counter]
            
            stimulus_counter += 1

            stimulus_info = prepare_stimulus(stimulus_descriptor, target)
            info_arr.value = bytes(str(stimulus_info[0] + '/' + str(stimulus_info[1][0]) + '/' + str(stimulus_info[1][1])), 'utf8')
            blank_timer = -1
            frame_float = 0
            prime_shown = False
            target_shown = False
            snapshot_taken = False
            target_time = -1

    # handle inputs
    if A_pressed and not prev_A_pressed:
        prev_A_pressed = A_pressed
    if not A_pressed and prev_A_pressed:
        prev_A_pressed = A_pressed
    if Sp_pressed and not prev_Sp_pressed:
        prev_Sp_pressed = Sp_pressed
    if not Sp_pressed and prev_Sp_pressed:
        prev_Sp_pressed = Sp_pressed

def snapshot_worker(snap, snap_null, terminate, info, reaction_time, corr, config):
    results_file_name = datetime.datetime.now().strftime("experiment_%Y-%m-%d_%H-%M-%S.csv")
    if len(sys.argv) > 1:
        results_file_name = results_file_name.split('.')[0] + '_' + sys.argv[1] + '.' + results_file_name.split('.')[1]
    try:
        file = open(os.path.join(config['results_dir'], results_file_name), 'w+')
        print('Using custom results directory.')
    except:
        file = open('results/' + results_file_name, 'w+')
    csv_writer = csv.writer(file)
    csv_writer.writerow(['prime_filename', 'grip_name', 'grip_orientation', 'target_inner_yellow', 'target_outer_yellow', 'is_target', 'reaction_time', 'hand_orientation_0', 'hand_orientation_1', 'hand_orientation_2'])
    file.flush()

    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles
    mp_hands = mp.solutions.hands
    hands = mp_hands.Hands(model_complexity=1, min_detection_confidence=0.5, min_tracking_confidence=0.5, max_num_hands=2)
    cap = cv2.VideoCapture(config['cam_index'])

    rows = []
    
    counter = 0

    while True:
        cam_success, cam_image = cap.read()

        if snap.value:
            if not snap_null.value:
                cam_success, cam_image = cap.read()
                if cam_success:
                    cam_image.flags.writeable = False
                    cam_image = cv2.cvtColor(cam_image, cv2.COLOR_BGR2RGB)
                    results = hands.process(cam_image)

                    cam_image.flags.writeable = True
                    cam_image = cv2.cvtColor(cam_image, cv2.COLOR_RGB2BGR)
                    if results.multi_hand_landmarks:
                        for hand_landmarks in results.multi_hand_landmarks:
                            points = np.array([
                                [hand_landmarks.landmark[0].x, 1 - hand_landmarks.landmark[0].y],
                                [hand_landmarks.landmark[5].x, 1 - hand_landmarks.landmark[5].y],
                                [hand_landmarks.landmark[9].x, 1 - hand_landmarks.landmark[9].y],
                                [hand_landmarks.landmark[13].x, 1 - hand_landmarks.landmark[13].y],
                                [hand_landmarks.landmark[17].x, 1 - hand_landmarks.landmark[17].y],
                                [hand_landmarks.landmark[6].x, 1 - hand_landmarks.landmark[6].y],
                                [hand_landmarks.landmark[10].x, 1 - hand_landmarks.landmark[10].y],
                                [hand_landmarks.landmark[14].x, 1 - hand_landmarks.landmark[14].y],
                                [hand_landmarks.landmark[18].x, 1 - hand_landmarks.landmark[18].y]
                            ])
                            orientations = [
                                np.arctan2(*((np.mean(points[1:5], axis=0)) - points[0])) / np.pi * 180,
                                np.mean(np.arctan2(*(points[6:8] - points[2:4]).T)) / np.pi * 180,
                                np.mean(np.arctan2(*(points[5:9] - points[1:5]).T)) / np.pi * 180,
                            ]
                            counter += 1
                            print(counter)
                            print(orientations)
                            
                            mp_drawing.draw_landmarks(
                                cam_image,
                                hand_landmarks,
                                mp_hands.HAND_CONNECTIONS,
                                mp_drawing_styles.get_default_hand_landmarks_style(),
                                mp_drawing_styles.get_default_hand_connections_style())
                    else:
                        orientations = ['null', 'null', 'null']
                                
                    cv2.imwrite('cam.png', cam_image)

                    stimulus_info_str = info.value.decode('utf8')
                    filename, inner_yellow, outer_yellow= stimulus_info_str.split('/')

                    if filename != 'control':
                        grip_name, orientation_str = re.search(r'([a-z]*)(-?\d*)', filename.split('.')[0]).groups()
                        grip_orientation = int(orientation_str)
                    else:
                        grip_name = 'control'
                        grip_orientation = 0

                    csv_writer.writerow([filename, grip_name, grip_orientation, inner_yellow, outer_yellow, 1 * (inner_yellow == outer_yellow), reaction_time.value, orientations[0], orientations[1], orientations[2]])
                    file.flush()

                    if (filename != 'control') and (inner_yellow == outer_yellow) and (orientations != ['null', 'null', 'null']):
                        rows.append([grip_orientation] + orientations)
                        if len(rows) > 1:
                            arr = np.array(rows)
                            arr = arr[np.logical_not(np.any(np.isnan(arr), axis=1))]
                            cov = np.cov(arr.T)
                            corr1 = cov[0, 1] / np.sqrt(cov[0, 0]) / np.sqrt(cov[1, 1])
                            corr2 = cov[0, 2] / np.sqrt(cov[0, 0]) / np.sqrt(cov[2, 2])
                            corr3 = cov[0, 3] / np.sqrt(cov[0, 0]) / np.sqrt(cov[3, 3])
                            corr[0] = corr1
                            corr[1] = corr2
                            corr[2] = corr3
                            try:
                                print()
                                print(len(arr), 'rows\ncorr', int(corr[0] * 1000) / 10, int(corr[1] * 1000) / 10, int(corr[2] * 1000) / 10, '\n')
                            except:
                                pass
            else:
                orientations = ['null', 'null', 'null']
                stimulus_info_str = info.value.decode('utf8')
                filename, inner_yellow, outer_yellow= stimulus_info_str.split('/')

                if filename != 'control':
                    grip_name, orientation_str = re.search(r'([a-z]*)(-?\d*)', filename.split('.')[0]).groups()
                    grip_orientation = int(orientation_str)
                else:
                    grip_name = 'control'
                    grip_orientation = 0

                csv_writer.writerow([filename, grip_name, grip_orientation, inner_yellow, outer_yellow, 1 * (inner_yellow == outer_yellow), 'null', orientations[0], orientations[1], orientations[2]])
                file.flush()

            snap.value = 0

        if terminate.value:
            cap.release()
            break

    file.close()

if __name__ == '__main__':

    with open('config.json', 'r') as f:
        config = json.load(f)

    pyglet.options['vsync'] = True

    display = pyglet.canvas.get_display()
    screens = display.get_screens()
    if len(screens) > 1:
        window = pyglet.window.Window(fullscreen=FULLSCREEN, screen=screens[config['screen']])
    else:
        window = pyglet.window.Window(fullscreen=FULLSCREEN, )
    stimulus_size = int(window.height * STIMULUS_RELATIVE_SIZE)

    images = []
    for fname in os.listdir('primes'):
        if fname in ['.gitkeep']:
            continue
        images.append((fname, ImageOps.flip(Image.open('primes/' + fname).resize((stimulus_size, stimulus_size)))))
    image_arrays = np.array([np.array(im[1]) for im in images])

    control_order = []
    for i in range(NUM_CONTROL_STIMULI):
        control_order.append(['c', np.random.rand() < TARGET_PROBABILITY])

    num_target_stimuli = len(images) * NUM_REPITITIONS
    num_non_target_stimuli = int(num_target_stimuli / TARGET_PROBABILITY * (1 - TARGET_PROBABILITY))
    num_non_target_stimuli -= num_non_target_stimuli % 4
    target_stimuli = [[i, True] for i in range(len(images))] * NUM_REPITITIONS
    non_target_stimuli = [[i, False] for i in np.random.choice(len(images), num_non_target_stimuli)]
    print('Number of target stimuli:', len(target_stimuli))
    print('Number of non-target stimuli:', len(non_target_stimuli))
    stimuli_order = target_stimuli + non_target_stimuli

    random.shuffle(stimuli_order)

    stimulus_counter = 0
    control_phase = True
    
    initial_screen = True
    control_finish_screen = False
    pause_screen = False
    thanks_screen = False

    prev_info_screen = False


    # Prepare static graphics
    cross_h = pyglet.shapes.Rectangle(window.width // 2 - stimulus_size // 6, window.height // 2 - stimulus_size // 70, stimulus_size // 3, stimulus_size // 35)
    cross_v = pyglet.shapes.Rectangle(window.width // 2 - stimulus_size // 70, window.height // 2 - stimulus_size // 6, stimulus_size // 35, stimulus_size // 3)
    circle_yellow_inner = pyglet.shapes.Circle(window.width // 2, window.height // 2, stimulus_size // 15, color=(255, 255, 0))
    circle_blue_inner = pyglet.shapes.Circle(window.width // 2, window.height // 2, stimulus_size // 15, color=(0, 0, 255))
    circle_yellow_outer = pyglet.shapes.Circle(window.width // 2, window.height // 2, stimulus_size // 2, color=(255, 255, 0))
    circle_blue_outer = pyglet.shapes.Circle(window.width // 2, window.height // 2, stimulus_size // 2, color=(0, 0, 255))
    circle_black = pyglet.shapes.Circle(window.width // 2, window.height // 2, int(stimulus_size * 0.4), color=(0, 0, 0))
    frame_text = pyglet.text.Label('0')
    noise_texture = pyglet.image.ImageData(stimulus_size, stimulus_size, "RGB", Image.open('masks/mask.JPG').resize((stimulus_size, stimulus_size)).tobytes(), stimulus_size * 3 * 1)
    control_noise_texture = pyglet.image.ImageData(stimulus_size, stimulus_size, "RGB", Image.open('masks/mask2.JPG').resize((stimulus_size, stimulus_size)).tobytes(), stimulus_size * 3 * 1)


    prime_texture = None
    inner_yellow_flag = None
    outer_yellow_flag = None

    frame_float = -1
    blank_timer = -1
    Enter_pressed = False
    A_pressed = False
    prev_A_pressed = False
    Sp_pressed = False
    prev_Sp_pressed = False
    prime_shown = False
    target_shown = False
    snapshot_taken = False
    target_time = -1
    skip_space = False


    @window.event
    def on_draw():
        window.clear()
        
        global frame_float, inner_yellow_flag, outer_yellow_flag, prime_shown, target_shown, initial_screen, control_finish_screen, pause_screen, thanks_screen

        if initial_screen:
            info_text = pyglet.text.Label('Zum Starten [ENTER] drücken.',
                font_size=36,
                x=window.width / 2, 
                y=window.height / 2,
                anchor_x='center',
                anchor_y='center')
            info_text.draw()
            return

        if control_finish_screen:
            info_text = pyglet.text.Label('Einführung abgeschlossen. Noch Fragen? [ENTER]',
                font_size=36,
                x=window.width / 2, 
                y=window.height / 2,
                anchor_x='center',
                anchor_y='center')
            info_text.draw()
            return

        if pause_screen:
            info_text = pyglet.text.Label('Pause. [ENTER]',
                font_size=36,
                x=window.width / 2, 
                y=window.height / 2,
                anchor_x='center',
                anchor_y='center')
            info_text.draw()
            return

        if thanks_screen:
            info_text_1 = pyglet.text.Label('Experiment abgeschlossen. Vielen Dank!',
                font_size=36,
                x=window.width / 2, 
                y=window.height / 2 + 90,
                anchor_x='center',
                anchor_y='center')
            info_text_1.draw()

            info_text_2 = pyglet.text.Label('Gesamt Reiz-Reaktions-Korrelation: ' + str(int(np.round(1000 * corr_arr[2])) / 10) + '%',
                font_size=36,
                x=window.width / 2, 
                y=window.height / 2 - 30,
                anchor_x='center',
                anchor_y='center')
            info_text_2.draw()

            corrs_to_compare = list(map(lambda t: t[2], pause_corrs)) + [corr_arr[2]]
            argmax = np.argmax(corrs_to_compare)
            info_text_3 = pyglet.text.Label('Stärkste Reiz-Reaktions-Korrelation: Abschnitt 1-' + str(1 + argmax) + ' mit ' + str(int(np.round(1000 * corrs_to_compare[argmax])) / 10) + '%',
                font_size=36,
                x=window.width / 2, 
                y=window.height / 2 - 90,
                anchor_x='center',
                anchor_y='center')
            info_text_3.draw()
            return


        frame = int(frame_float)

        if FRAME_TEXT:
            frame_text.text = str(frame)
            frame_text.draw()

        if frame == -1:
            return
        
        if frame < 24:
            cross_h.draw()
            cross_v.draw()
        
        if frame >= 24 and (frame - 24) < 13 and (frame - 24) != 6:
            noise_texture.blit(window.width//2-stimulus_size//2, window.height//2-stimulus_size//2)

        if (frame - 24) == 6:
            prime_texture.blit(window.width//2-stimulus_size//2, window.height//2-stimulus_size//2)
            prime_shown = True

        if frame >= 24 + 13:
            if outer_yellow_flag:
                circle_yellow_outer.draw()
            else:
                circle_blue_outer.draw()
            circle_black.draw()
            if inner_yellow_flag:
                circle_yellow_inner.draw()
            else:
                circle_blue_inner.draw()
            target_shown = True
                                
        if frame >= 0 and frame < 100:
            frame_float = frame_float + TIME_SCALE

    @window.event
    def on_key_press(symbol, modifier):
        global A_pressed, prev_A_pressed, Sp_pressed, prev_Sp_pressed, Enter_pressed
        if symbol == pyglet.window.key.A:
            A_pressed = True

        if symbol == pyglet.window.key.SPACE:
            Sp_pressed = True

        if symbol == pyglet.window.key.ENTER:
            Enter_pressed = True

    @window.event
    def on_key_release(symbol, modifier):
        global A_pressed, prev_A_pressed, Sp_pressed, prev_Sp_pressed, Enter_pressed
        if symbol == pyglet.window.key.A:
            A_pressed = False

        if symbol == pyglet.window.key.SPACE:
            Sp_pressed = False

        if symbol == pyglet.window.key.ENTER:
            Enter_pressed = False

    snap_val = multiprocessing.Value('i', 0)
    snap_null_val = multiprocessing.Value('i', 0)
    terminate_val = multiprocessing.Value('i', 0)
    reaction_time_val = multiprocessing.Value('i', 0)
    info_arr = multiprocessing.Array('c', bytes(' ' * 100, 'utf8'))
    corr_arr = multiprocessing.Array('f', [0, 0, 0])

    pause_corrs = []

    p = multiprocessing.Process(target=snapshot_worker, args=(snap_val, snap_null_val, terminate_val, info_arr, reaction_time_val, corr_arr, config))
    p.start()

    pyglet.clock.schedule_interval(update, 1./240.)

    pyglet.app.run()

    terminate_val.value = 1
    p.join()
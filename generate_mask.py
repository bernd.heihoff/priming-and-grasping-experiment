import numpy as np
from PIL import Image
import os

stimulus_size = 1000
images = []
for fname in os.listdir('primes'):
    if fname == '.gitkeep' in fname:
        continue
    images.append((fname, Image.open('primes/' + fname).resize((stimulus_size, stimulus_size))))
image_arrays = np.array([np.array(im[1]) for im in images])


noise_ix = np.zeros((1, 25, 25, 3)).astype(int)
noise_ix[:,:,:,0] = np.random.choice(len(images), noise_ix.shape[:3])
noise_ix[:,:,:,1:] = np.random.choice(25, noise_ix.shape[:3] + (2,))
noise = np.zeros((1, stimulus_size, stimulus_size, 3)).astype(np.uint8)
for n_ix in np.ndindex(noise_ix.shape):
    n_sl = noise[n_ix[0], (stimulus_size // 25 * n_ix[1]):(stimulus_size // 25 * (n_ix[1] + 1)), (stimulus_size // 25 * n_ix[2]):(stimulus_size // 25 * (n_ix[2] + 1))]
    im_ix = noise_ix[n_ix[0], n_ix[1], n_ix[2]]
    im_sl = image_arrays[im_ix[0], (stimulus_size // 25 * im_ix[1]):(stimulus_size // 25 * (im_ix[1] + 1)), (stimulus_size // 25 * im_ix[2]):(stimulus_size // 25 * (im_ix[2] + 1))]
    n_sl[:] = im_sl

Image.fromarray(noise[0]).save('masks/mask.JPG')
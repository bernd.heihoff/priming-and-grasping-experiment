Das Ausführen dieses Experiments erfordert die Installation von Python 3.

Installation der Abhängigkeiten: 
pip install -r requirements.txt

Der Inhalt der Datei config.json.template muss in eine neue Datei config.json kopiert werden,
die Parameter müssen gegebenenfalls angepasst werden:

  cam_index: Ganzzahl >= 0; gibt den Index der zu nutzenden Kamera an.
  screen: Ganzzahl >= 0; gibt den Index des zu nutzenden Monitors an.
  results_dir: Zeichenkette; gibt den Speicherort der zu erzeugenden .csv-Dateien an

Ausführen des Hauptprogramms:
python experiment.py
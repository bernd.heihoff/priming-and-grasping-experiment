import cv2

ORIGINAL_PATH = '../GriffeFotos'
CUTOUT_PATH = 'primes'

WIDTH = 3456
HEIGHT = 2304

FILES = ['gelb-15.JPG', 'gelb-30.JPG', 'gelb-45.JPG', 'gelb-60.JPG', 'gelb0.JPG', 'gelb15.JPG', 'gelb30.JPG', 'gelb45.JPG', 'gelb60.JPG', 'orange-15.JPG', 'orange-30.JPG', 'orange-45.JPG', 'orange0.JPG', 'orange15.JPG', 'orange30.JPG', 'orange45.JPG', 'orange60.JPG', 'weiss-15.JPG', 'weiss-30.JPG', 'weiss-45.JPG', 'weiss-60.JPG', 'weiss0.JPG', 'weiss15.JPG', 'weiss30.JPG', 'weiss45.JPG', 'weiss60.JPG']
HORIZONTAL_CENTERS = [1788, 1788, 1788, 1788, 1744, 1788, 1788, 1764, 1788, 1768, 1768, 1752, 1752, 1724, 1724, 1724, 1724, 1712, 1720, 1728, 1708, 1720, 1712, 1760, 1724, 1724]

padding = HEIGHT // 2

for (file, center_x) in zip(FILES, HORIZONTAL_CENTERS):
    img = cv2.imread(f'{ORIGINAL_PATH}/{file}')

    new_center_x = max(padding, min(WIDTH - padding, center_x))
    img = img[:, new_center_x-padding:new_center_x+padding]

    cv2.imwrite(f'{CUTOUT_PATH}/{file}', img)
